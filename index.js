const Discord = require('discord.js')
const bot = new Discord.Client()
const prefix = "?"


//Id of the Admin


var admin = '407891319965614100'


//Id of the waiting role


var enAttente = '458591052945948683'


//Id of the accueil channel


var accueilChannel = '407492327397916675'

//Id of the annoucement channels


var oshimoAnnoucementChannel = '491141602098675712'
var terraAnnoucementChannel = '489782218395222016'
var herdegrizeAnnoucementChannel = '491141879094706176'


//Id of the server roles


var oshimoRole = '513701200575725568'
var terraRole = '513701407652577281'
var herdegrizeRole = '513701581540163587'


//Id of the Chef roles


var chefoA = '407891662816280576'
var chefoB = '407896949744861187'
var chefoC = '407896827703328788'
var chefoD = '407897219639934977'
var cheftA = '408172497381490698'
var cheftB = '408172804748476416'
var cheftC = '408173482883416074'
var cheftD = '408173775197044736'
var chefhA = '408175389152641025'
var chefhB = '408175605486583808'
var chefhC = '408175780019961856'
var chefhD = '408176147885719552'


//Id of the global server chef roles


var chefSalleO = '504353911264772097'
var chefSalleT = '505385042806046721'
var chefSalleH = '504362357275885569'


//Id of the chef room


var chefRoomO = '491142031268118539'
var chefRoomT = '499104283015512064'
var chefRoomH = '491142185954050049'


//Id of the room role


var salleABO = '506110215649755147'
var salleCDO = '506110219127095316'
var salleABT = '505695574344073217'
var salleCDT = '505695578081067029'
var salleABH = '504370546386010122'
var salleCDH = '504370643027099648'


// Id of the chief channel

var devenirChef = '504909224518615069'
var listeCommandeChefs = '505269255202275328'

//Validation of date by chef 


var chefoaValid = false
var chefobValid = false
var chefocValid = false
var chefodValid = false
var cheftaValid = false
var cheftbValid = false
var cheftcValid = false
var cheftdValid = false
var chefhaValid = false
var chefhbValid = false
var chefhcValid = false
var chefhdValid = false


//Date of oppening


var dateO = "Il n'y a pas de date d'ouverture prévue pour l'instant."
var dateOprov = ""
var dateT = "Il n'y a pas de date d'ouverture prévue pour l'instant."
var dateTprov = ""
var dateH = "Il n'y a pas de date d'ouverture prévue pour l'instant."
var dateHprov = ""


//Message for Chef de salle


var chefOshimoMessage = "**RÉGLEMENT** \n \n:red_circle: Le choix de la date et l’heure se fait **unanimement** entre les 4 chefs grâce aux commandes <#" + listeCommandeChefs + "> .\n \n:red_circle: 4 jours mini requis entre le choix de la date et l’ouverture.\n \n:red_circle: Une fois la date validée : **UN SEUL TAG @OSHIMO** sur le discord.\n \n:red_circle: Il est conseillé de diffuser l’information IG et plus largement sur les réseaux sociaux (Discord, Twitter, Forum).\n \n:red_circle: Au jour et à l’heure de l’ouverture : **UN SEUL TAG @OSHIMO ** sur le discord.\n \n:red_circle: *Durant l’ouverture* : gardez contact (groupé ou vocal) avec les autres chefs.\n \n:red_circle: *Durant l’ouverture* : gardez contact avec les joueurs de votre salle. **Soyez positif** sur l’avancement des autres salles, proposez leurs de participer au recrutement.\n \n:red_circle: La salle pleine, aidez les autres chefs en copiant leurs messages de recrutement.\n \n:red_circle: Assurez vous d’avoir un ami devant le donjon. Il  vous dira lorsque l’Antre ouvre. **Les joueurs dans votre salle ne doivent pas bouger avant votre signal.**\n \n:red_circle: Une fois l’Antre ouverte : **UN SEUL TAG @OSHIMO ** sur le discord.\n \n__**VOTRE RÔLE AU MOMENT DE L’OUVERTURE**__\n \n:one:  Spam le canal recrutement en jeu avec des messages du type :\n \n*OUVERTURE KRALA : recrute Xel/Cra M et Eni F en %pos%*\n \n:two: Spam les canaux alliance et guilde mais aussi les amis IG avec le même genre de message.\n \n*NOTA : Il est préférable d'avoir une bonne alliance qui accepte d'aider et qui soit au courant en avance.*\n \n:three: Si possible faire la même demande sur Discord Krala **SANS TAG** et sur discord DT\n \n:four:  Ne rien prévoir IRL durant les 3 heures qui suivent l’heure de démarrage pour ouvrir et faire le donjon.\n \n:five: Communiquer avec les autres chefs, quand un chef a fini sa salle, il va aider à recruter pour les autres.\n \n**Retrouvez ces informations sur <#" + devenirChef + ">.** \n**Respectez vous et soyez positif !**\n**Smile and Enjoy !**"
var chefTerraMessage = "**RÉGLEMENT** \n \n:red_circle: Le choix de la date et l’heure se fait **unanimement** entre les 4 chefs  grâce aux commandes <#" + listeCommandeChefs + ">.\n \n:red_circle: 4 jours mini requis entre le choix de la date et l’ouverture.\n \n:red_circle: Une fois la date validée : **UN SEUL TAG @TERRA-COGITA** sur le discord.\n \n:red_circle: Il est conseillé de diffuser l’information IG et plus largement sur les réseaux sociaux (Discord, Twitter, Forum).\n \n:red_circle: Au jour et à l’heure de l’ouverture : **UN SEUL TAG @TERRA-COGITA ** sur le discord.\n \n:red_circle: *Durant l’ouverture* : gardez contact (groupé ou vocal) avec les autres chefs.\n \n:red_circle: *Durant l’ouverture* : gardez contact avec les joueurs de votre salle. **Soyez positif** sur l’avancement des autres salles, proposez leurs de participer au recrutement.\n \n:red_circle: La salle pleine, aidez les autres chefs en copiant leurs messages de recrutement.\n \n:red_circle: Assurez vous d’avoir un ami devant le donjon. Il  vous dira lorsque l’Antre ouvre. **Les joueurs dans votre salle ne doivent pas bouger avant votre signal.**\n \n:red_circle: Une fois l’Antre ouverte : **UN SEUL TAG @ TERRA-COGITA ** sur le discord.\n \n__**VOTRE RÔLE AU MOMENT DE L’OUVERTURE**__\n \n:one:  Spam le canal recrutement en jeu avec des messages du type :\n \n*OUVERTURE KRALA : recrute Xel/Cra M et Eni F en %pos%*\n \n:two: Spam les canaux alliance et guilde mais aussi les amis IG avec le même genre de message.\n \n*NOTA : Il est préférable d'avoir une bonne alliance qui accepte d'aider et qui soit au courant en avance.*\n \n:three: Si possible faire la même demande sur Discord Krala **SANS TAG** et sur discord DT\n \n:four:  Ne rien prévoir IRL durant les 3 heures qui suivent l’heure de démarrage pour ouvrir et faire le donjon.\n \n:five: Communiquer avec les autres chefs, quand un chef a fini sa salle, il va aider à recruter pour les autres.\n \n**Retrouvez ces informations sur <#" + devenirChef + ">.** \n**Respectez vous et soyez positif !**\n**Smile and Enjoy !**"
var chefHerdegrizeMessage = "**RÉGLEMENT** \n \n:red_circle: Le choix de la date et l’heure se fait **unanimement** entre les 4 chefs  grâce aux commandes <#" + listeCommandeChefs + ">.\n \n:red_circle: 4 jours mini requis entre le choix de la date et l’ouverture.\n \n:red_circle: Une fois la date validée : **UN SEUL TAG @HERDEGRIZE** sur le discord.\n \n:red_circle: Il est conseillé de diffuser l’information IG et plus largement sur les réseaux sociaux (Discord, Twitter, Forum).\n \n:red_circle: Au jour et à l’heure de l’ouverture : **UN SEUL TAG @HERDEGRIZE ** sur le discord.\n \n:red_circle: *Durant l’ouverture* : gardez contact (groupé ou vocal) avec les autres chefs.\n \n:red_circle: *Durant l’ouverture* : gardez contact avec les joueurs de votre salle. **Soyez positif** sur l’avancement des autres salles, proposez leurs de participer au recrutement.\n \n:red_circle: La salle pleine, aidez les autres chefs en copiant leurs messages de recrutement.\n \n:red_circle: Assurez vous d’avoir un ami devant le donjon. Il  vous dira lorsque l’Antre ouvre. **Les joueurs dans votre salle ne doivent pas bouger avant votre signal.**\n \n:red_circle: Une fois l’Antre ouverte : **UN SEUL TAG @HERDEGRIZE ** sur le discord.\n \n__**VOTRE RÔLE AU MOMENT DE L’OUVERTURE**__\n \n:one:  Spam le canal recrutement en jeu avec des messages du type :\n \n*OUVERTURE KRALA : recrute Xel/Cra M et Eni F en %pos%*\n \n:two: Spam les canaux alliance et guilde mais aussi les amis IG avec le même genre de message.\n \n*NOTA : Il est préférable d'avoir une bonne alliance qui accepte d'aider et qui soit au courant en avance.*\n \n:three: Si possible faire la même demande sur Discord Krala **SANS TAG** et sur discord DT\n \n:four:  Ne rien prévoir IRL durant les 3 heures qui suivent l’heure de démarrage pour ouvrir et faire le donjon.\n \n:five: Communiquer avec les autres chefs, quand un chef a fini sa salle, il va aider à recruter pour les autres.\n \n **Retrouvez ces informations sur <#" + devenirChef + ">.** \n**Respectez vous et soyez positif !**\n**Smile and Enjoy !**"


//Global welcome message


var welcomeMessage = " et bienvenue sur le discord pour l'ouverture du Kralamoure Géant. Pour avoir accès aux différents canaux, voici comment procéder :\n Si ton personnage fait partie de cette liste :\n• Sram Mâle\n• Féca Mâle\n• Iop Mâle\n• Sadida Femelle\n• Enutrof Femelle\n• Pandawa Femelle\n• Eniripsa Mâle\n• Ecaflip Mâle\n• Sacrieur Mâle\n• Osamodas Femelle\n• Xélor Femelle\n• Crâ Femelle\n \nAlors tape **?serveur-1** dans le chat\n**Exemple :** Je suis une __Xelor Femelle__ du serveur __Herdegrize__, je tape :```?herdegrize-1```\n \nSi votre personnage **ne fait pas partie de cette liste**, tapez **?serveur-2**\n**Exemple :** Je suis une __Iop Femelle__ du serveur __Terra Cogita__, je tape :```?terra-cogita-2```"

//Set game of the bot


bot.on('ready', function () {
    bot.user.setActivity('Chercher des ogrines').catch(console.error)
})


//Test commands


bot.on('message', function (message) {
    if (message.content === 'qui es-tu?') {
        message.channel.send("Je m'apelle Otomaï et je suis là pour vous aider")
    }
})

//Post message when new user come


bot.on('guildMemberAdd', member => {
    member.guild.channels.get('407492327397916675').send("Bonjour <@" + member.id + ">" + welcomeMessage);
    member.addRole(enAttente)
    member.createDM().then(channel => {
        return channel.send('Bonjour ' + welcomeMessage)
    }).catch(console.error)
});


//Post message when user leave


bot.on('guildMemberRemove', member => {
    member.guild.channels.get('413437305341214730').send("**" + member.user.username + "#" + member.user.discriminator + "** (<@" + member.id + ">) à quitté le serveur.");
});


bot.on("message", message => {


    //Check if there is an unhautorized command in the welcome channel


    if (message.channel.id == accueilChannel && message.content.substring(0, 1) == prefix) {

        if (message.content == prefix + "oshimo-1" || message.content == prefix + "oshimo-2" || message.content == prefix + "terra-cogita-1" || message.content == prefix + "terra-cogita-2" || message.content == prefix + "herdegrize-1" || message.content == prefix + "herdegrize-2") {
        } else {
            message.delete()
        }
    }


    //Command for verify if the bot is on


    if (message.content == 'test') {
        message.channel.send("La version dev d'Otomaï est dans la place!");
    }


    //command vor verify if some people have the waiting role with another role or haven't any role


    if (message.content == 'verifyRole') {
        var countWithout = 0
        var countError = 0
        message.guild.members.forEach(member => {

            if (member.roles.size === 1) {
                member.addRole(enAttente)
                countWithout = countWithout + 1
            }
            if (member.roles.size > 2 && member.roles.has(enAttente)) {
                member.removeRole(enAttente)
                countError = countError + 1
            }
        });
        if (countWithout === 0) {
            message.channel.send("Aucun membre n'a pas de rôle")

        } else {
            message.channel.send("j'ai rajouter le role en attente à " + countWithout + " personnes")
        }
        if (countError === 0) {
            message.channel.send("Personne n'a le rôle en attente en plus d'un autre")
        } else {
            message.channel.send("j'ai enlever le role en attente à " + countError + " personnes")
        }
    }


    //Show all the role of the guild and the number of people in each role


    if (message.content == prefix + 'roles') {
        var roles = message.guild.roles
        sorted = roles.sort((role1, role2) => role2.position - role1.position)

        messages = "```"
        sorted.forEach(role => {

            roleName = role.name.padEnd(30)

            messages = messages + roleName + role.members.map(m => m.user.id).length + " membres" + ('\n');

        });
        messages = messages + "```"
        message.channel.send(messages);
    }


    //Reply if the user write ?serveur


    if (message.content.substring(0, 8) == prefix + "serveur" || message.content.substring(0, 10) == "je suis un" || message.content.substring(0, 10) == "Je suis un" || message.content.substring(0, 2) == "? ") {
        message.channel.send("Fais pas ton Bwork, suis les exemples !")
    }


    //Command for give basic role




    if (message.member.roles.has(enAttente)) {
        if (message.content == prefix + "oshimo-1") {
            message.member.addRoles([salleABO, oshimoRole])
            message.member.removeRole(enAttente)
            message.channel.send("Bienvenue, ton rôle a été modifié avec succès.")
        }


        if (message.content == prefix + "oshimo-2") {
            message.member.addRoles([salleCDO, oshimoRole])
            message.member.removeRole(enAttente)
            message.channel.send("Bienvenue, ton rôle a été modifié avec succès.")
        }


        if (message.content == prefix + "terra-cogita-1") {
            message.member.addRoles([salleABT, terraRole])
            message.member.removeRole(enAttente)
            message.channel.send("Bienvenue, ton rôle a été modifié avec succès.")
        }


        if (message.content == prefix + "terra-cogita-2") {
            message.member.addRoles([salleCDT, terraRole])
            message.member.removeRole(enAttente)
            message.channel.send("Bienvenue, ton rôle a été modifié avec succès.")
        }


        if (message.content == prefix + "herdegrize-1") {
            message.member.addRoles([salleABH, herdegrizeRole])
            message.member.removeRole(enAttente)
            message.channel.send("Bienvenue, ton rôle a été modifié avec succès.")
        }


        if (message.content == prefix + "herdegrize-2") {
            message.member.addRoles([salleCDH, herdegrizeRole])
            message.member.removeRole(enAttente)
            message.channel.send("Bienvenue, ton rôle a été modifié avec succès.")
        }

    } else {
        if (message.content == prefix + "oshimo-1") {
            if (message.member.roles.has(salleABO)) {
                message.channel.send("Vous avez déjà ce rôle")
            } else {
                message.member.addRole(salleABO)
                message.channel.send("Je vous ai rajouté vos rôles")
            }
        }


        if (message.content == prefix + "oshimo-2") {
            if (message.member.roles.has(salleCDO)) {
                message.channel.send("Vous avez déjà ce rôle")
            } else {
                message.member.addRole(salleCDO)
                message.channel.send("Je vous ai rajouté vos rôles")
            }
        }


        if (message.content == prefix + "terra-cogita-1") {
            if (message.member.roles.has(salleABT)) {
                message.channel.send("Vous avez déjà ce rôle")
            } else {
                message.member.addRole(salleABT)
                message.channel.send("Je vous ai rajouté vos rôles")
            }
        }


        if (message.content == prefix + "terra-cogita-2") {
            if (message.member.roles.has(salleCDT)) {
                message.channel.send("Vous avez déjà ce rôle")
            } else {
                message.member.addRoles(salleCDT)
                message.channel.send("Je vous ai rajouté vos rôles")
            }
        }


        if (message.content == prefix + "herdegrize-1") {
            if (message.member.roles.has(salleABH)) {
                message.channel.send("Vous avez déjà ce rôle")
            } else {
                message.member.addRole(salleABH)
                message.channel.send("Je vous ai rajouté vos rôles")
            }
        }


        if (message.content == prefix + "herdegrize-2") {
            if (message.member.roles.has(salleCDH)) {
                message.channel.send("Vous avez déjà ce rôle")
            } else {
                message.member.addRoles(salleCDH)
                message.channel.send("Je vous ai rajouté vos rôles")
            }
        }
    }


    //Chef de salle Oshimo view command


    if (message.content == prefix + 'chefo') {
        var chefoa = message.guild.roles.get(chefoA).members.map(m => m.user.id)
        var chefoan = message.guild.roles.get(chefoA).members.map(m => m.user.username)
        var chefob = message.guild.roles.get(chefoB).members.map(m => m.user.id)
        var chefobn = message.guild.roles.get(chefoB).members.map(m => m.user.username)
        var chefoc = message.guild.roles.get(chefoC).members.map(m => m.user.id)
        var chefocn = message.guild.roles.get(chefoC).members.map(m => m.user.username)
        var chefod = message.guild.roles.get(chefoD).members.map(m => m.user.id)
        var chefodn = message.guild.roles.get(chefoD).members.map(m => m.user.username)
        const ListEmbed = new Discord.RichEmbed()



        if (chefoa.length + chefob.length + chefoc.length + chefod.length === 4) {

            ListEmbed.setTitle('Chefs de salle OSHIMO :')
            ListEmbed.setDescription("Salle A : " + chefoan + " <@" + chefoa + ">." + ('\n') + "Salle B : " + chefobn + " <@" + chefob + ">." + ('\n') + "Salle C : " + chefocn + " <@" + chefoc + ">." + ('\n') + "Salle D : " + chefodn + " <@" + chefod + ">." + ('\n'));
            message.channel.send("Les chefs d'oshimo sont au complet !")
        }

        else {

            ListEmbed.setTitle('Chefs de salle OSHIMO :')



            if (chefoa.length === 0) {
                ListEmbed.setDescription("Il n'y a pas de chef de salle A." + ('\n'));
            } else {
                ListEmbed.setDescription("Il y a " + chefoa.length + " chef de salle A : " + chefoan + " (<@" + chefoa + ">)." + ('\n'));
            }

            if (chefob.length === 0) {
                ListEmbed.setDescription(ListEmbed.description + "Il n'y a pas de chef de salle B." + ('\n'));
            } else {
                ListEmbed.setDescription(ListEmbed.description + "Il y a " + chefob.length + " chef de salle B : " + chefobn + " (<@" + chefob + ">)." + ('\n'));
            }

            if (chefoc.length === 0) {
                ListEmbed.setDescription(ListEmbed.description + "Il n'y a pas de chef de salle C." + ('\n'));
            } else {
                ListEmbed.setDescription(ListEmbed.description + "Il y a " + chefoc.length + " chef de salle C : " + chefocn + " (<@" + chefoc + ">)." + ('\n'));
            }

            if (chefod.length === 0) {
                ListEmbed.setDescription(ListEmbed.description + "Il n'y a pas de chef de salle D." + ('\n'));
            } else {
                ListEmbed.setDescription(ListEmbed.description + "Il y a " + chefod.length + " chef de salle D : " + chefodn + " (<@" + chefod + ">)." + ('\n'));
            }
        }
        message.channel.send(ListEmbed);
    }


    //Chef de salle Terra Cogita view command


    if (message.content == prefix + 'cheft') {
        var chefta = message.guild.roles.get(cheftA).members.map(m => m.user.id)
        var cheftan = message.guild.roles.get(cheftA).members.map(m => m.user.username)
        var cheftb = message.guild.roles.get(cheftB).members.map(m => m.user.id)
        var cheftbn = message.guild.roles.get(cheftB).members.map(m => m.user.username)
        var cheftc = message.guild.roles.get(cheftC).members.map(m => m.user.id)
        var cheftcn = message.guild.roles.get(cheftC).members.map(m => m.user.username)
        var cheftd = message.guild.roles.get(cheftD).members.map(m => m.user.id)
        var cheftdn = message.guild.roles.get(cheftD).members.map(m => m.user.username)
        const ListEmbed = new Discord.RichEmbed()


        if (chefta.length + cheftb.length + cheftc.length + cheftd.length === 4) {

            ListEmbed.setTitle('Chefs de salle TERRA COGITA :')
            ListEmbed.setDescription("Salle A : " + cheftan + "  <@" + chefta + ">." + ('\n') + "Salle B : " + cheftbn + "  <@" + cheftb + ">." + ('\n') + "Salle C : " + cheftcn + "  <@" + cheftc + ">." + ('\n') + "Salle D : " + cheftdn + "  <@" + cheftd + ">." + ('\n'));
            message.channel.send("Les chefs sont au complet !")
        }

        else {

            ListEmbed.setTitle('Chefs de salle TERRA COGITA :')



            if (chefta.length === 0) {
                ListEmbed.setDescription("Il n'y a pas de chef de salle A." + ('\n'));
            } else {
                ListEmbed.setDescription("Il y a " + chefta.length + " chef de salle A : " + cheftan + "  (<@" + chefta + ">)." + ('\n'));
            }

            if (cheftb.length === 0) {
                ListEmbed.setDescription(ListEmbed.description + "Il n'y a pas de chef de salle B." + ('\n'));
            } else {
                ListEmbed.setDescription(ListEmbed.description + "Il y a " + cheftb.length + " chef de salle B :  " + cheftbn + "  (<@" + cheftb + ">)." + ('\n'));
            }

            if (cheftc.length === 0) {
                ListEmbed.setDescription(ListEmbed.description + "Il n'y a pas de chef de salle C." + ('\n'));
            } else {
                ListEmbed.setDescription(ListEmbed.description + "Il y a " + cheftc.length + " chef de salle C : " + cheftcn + "  (<@" + cheftc + ">)." + ('\n'));
            }

            if (cheftd.length === 0) {
                ListEmbed.setDescription(ListEmbed.description + "Il n'y a pas de chef de salle D." + ('\n'));
            } else {
                ListEmbed.setDescription(ListEmbed.description + "Il y a " + cheftd.length + " chef de salle D : " + cheftdn + "  (<@" + cheftd + ">)." + ('\n'));
            }
        }
        message.channel.send(ListEmbed);
    }


    //Chef de salle Herdegrize view command


    if (message.content == prefix + 'chefh') {
        var chefha = message.guild.roles.get(chefhA).members.map(m => m.user.id)
        var chefhan = message.guild.roles.get(chefhA).members.map(m => m.user.username)
        var chefhb = message.guild.roles.get(chefhB).members.map(m => m.user.id)
        var chefhbn = message.guild.roles.get(chefhB).members.map(m => m.user.username)
        var chefhc = message.guild.roles.get(chefhC).members.map(m => m.user.id)
        var chefhcn = message.guild.roles.get(chefhC).members.map(m => m.user.username)
        var chefhd = message.guild.roles.get(chefhD).members.map(m => m.user.id)
        var chefhdn = message.guild.roles.get(chefhD).members.map(m => m.user.username)
        const ListEmbed = new Discord.RichEmbed()


        if (chefha.length + chefhb.length + chefhc.length + chefhd.length === 4) {
            message.channel.send("Les chefs sont au complet !")
            ListEmbed.setTitle('Chefs de salle HERDEGRIZE :')
            ListEmbed.setDescription("Salle A : " + chefhan + "  <@" + chefha + ">." + ('\n') + "Salle B : " + chefhbn + "  <@" + chefhb + ">." + ('\n') + "Salle C : " + chefhbn + "  <@" + chefhc + ">." + ('\n') + "Salle D : " + chefhdn + "  <@" + chefhd + ">." + ('\n'));
        }

        else {

            ListEmbed.setTitle('Chefs de salle HERDEGRIZE :')



            if (chefha.length === 0) {
                ListEmbed.setDescription("Il n'y a pas de chef de salle A." + ('\n'));
            } else {
                ListEmbed.setDescription("Il y a " + chefha.length + " chef de salle A : " + chefhan + "  (<@" + chefha + ">)." + ('\n'));
            }

            if (chefhb.length === 0) {
                ListEmbed.setDescription(ListEmbed.description + "Il n'y a pas de chef de salle B." + ('\n'));
            } else {
                ListEmbed.setDescription(ListEmbed.description + "Il y a " + chefhb.length + " chef de salle B : " + chefhbn + "  (<@" + chefhb + ">)." + ('\n'));
            }

            if (chefhc.length === 0) {
                ListEmbed.setDescription(ListEmbed.description + "Il n'y a pas de chef de salle C." + ('\n'));
            } else {
                ListEmbed.setDescription(ListEmbed.description + "Il y a " + chefhc.length + " chef de salle C : " + chefhcn + "  (<@" + chefhc + ">)." + ('\n'));
            }

            if (chefhd.length === 0) {
                ListEmbed.setDescription(ListEmbed.description + "Il n'y a pas de chef de salle D." + ('\n'));
            } else {
                ListEmbed.setDescription(ListEmbed.description + "Il y a " + chefhd.length + " chef de salle D : " + chefhdn + "  (<@" + chefhd + ">)." + ('\n'));
            }
        }
        message.channel.send(ListEmbed);
    }


    //Chef de salle Oshimo acces commands


    if (message.content.substr(0, 7) == prefix + "chefo-") {

        if (message.content == prefix + "chefo-a") {
            var chefoa = message.guild.roles.get(chefoA).members.map(m => m.user.id)

            if (chefoa.length === 0 && message.member.roles.has(salleABO)) {
                message.channel.send("Bienvenue à notre nouveau chef de la salle A !")
                message.member.send(chefOshimoMessage)
                message.member.addRoles(chefoA).catch(console.error);
                message.member.addRoles(chefSalleO).catch(console.error);
            } else if (message.member.roles.has(salleABO)) {
                message.channel.send("Il y à déjà un chef de la salle A")
            } else {
                message.channel.send("Vous ne remplissez pas les conditions pour devenir le chef de la salle A. Pour plus d'info, consultez la <#504933472536887297>")
            }
        }

        if (message.content == prefix + "chefo-b") {
            var chefob = message.guild.roles.get(chefoB).members.map(m => m.user.id)

            if (chefob.length === 0 && message.member.roles.has(salleABO)) {
                message.channel.send("Bienvenue à notre nouveau chef de la salle B !")
                message.member.send(chefOshimoMessage)
                message.member.addRoles(chefoB).catch(console.error);
                message.member.addRoles(chefSalleO).catch(console.error);
            } else if (message.member.roles.has(salleABO)) {
                message.channel.send("Il y à déjà un chef de la salle B")
            } else {
                message.channel.send("Vous ne remplissez pas les conditions pour devenir le chef de la salle B. Pour plus d'info, consultez la <#504933472536887297>")
            }
        }

        if (message.content == prefix + "chefo-c") {
            var chefoc = message.guild.roles.get(chefoC).members.map(m => m.user.id)

            if (chefoc.length === 0 && message.member.roles.has(salleCDO)) {
                message.channel.send("Bienvenue à notre nouveau chef de la salle C !")
                message.member.send(chefOshimoMessage)
                message.member.addRoles(chefoC).catch(console.error);
                message.member.addRoles(chefSalleO).catch(console.error);
            } else if (message.member.roles.has(salleCDO)) {
                message.channel.send("Il y à déjà un chef de la salle C")
            } else {
                message.channel.send("Vous ne remplissez pas les conditions pour devenir le chef de la salle C. Pour plus d'info, consultez la <#504933472536887297>")
            }
        }

        if (message.content == prefix + "chefo-d") {
            var chefod = message.guild.roles.get(chefoD).members.map(m => m.user.id)

            if (chefod.length === 0 && message.member.roles.has(salleCDO)) {
                message.channel.send("Bienvenue à notre nouveau chef de la salle D !")
                message.member.send(chefOshimoMessage)
                message.member.addRoles(chefoD).catch(console.error);
                message.member.addRoles(chefSalleO).catch(console.error);
            } else if (message.member.roles.has(salleCDO)) {
                message.channel.send("Il y à déjà un chef de la salle D")
            } else {
                message.channel.send("Vous ne remplissez pas les conditions pour devenir le chef de la salle D. Pour plus d'info, consultez la <#504933472536887297>")
            }
        }
        var chefoa = message.guild.roles.get(chefoA).members.map(m => m.user.id)
        var chefob = message.guild.roles.get(chefoB).members.map(m => m.user.id)
        var chefoc = message.guild.roles.get(chefoC).members.map(m => m.user.id)
        var chefod = message.guild.roles.get(chefoD).members.map(m => m.user.id)
        if (chefoa.length + chefob.length + chefoc.length + chefod.length == 3) {
            message.member.guild.channels.get(chefRoomO).send("<@&" + chefSalleO + "> Vous êtes au complet, il faut donc choisir une date d'ouverture ! Pour proposer une date utilisez la commande `?setDate-o (date et heure)`");
            message.channel.send("vous êtes complets")
        }
    }

    if (message.content == prefix + "fino") {


        if (message.member.roles.has(chefoA)) {
            message.member.send("Merci pour votre investissement :wink:")
            message.channel.send(message.member.user.username + " a quitté son poste de chef de la salle A d'OSHIMO, merci à lui.")
            message.member.removeRoles(chefoA).catch(console.error);
            message.member.removeRoles(chefSalleO).catch(console.error);
        }


        if (message.member.roles.has(chefoB)) {
            message.member.send("Merci pour votre investissement :wink:")
            message.channel.send(message.member.user.username + " a quitté son poste de chef de la salle B d'OSHIMO, merci à lui.")
            message.member.removeRoles(chefoB).catch(console.error);
            message.member.removeRoles(chefSalleO).catch(console.error);
        }


        if (message.member.roles.has(chefoC)) {
            message.member.send("Merci pour votre investissement :wink:")
            message.channel.send(message.member.user.username + " a quitté son poste de chef de la salle C d'OSHIMO, merci à lui.")
            message.member.removeRoles(chefoC).catch(console.error);
            message.member.removeRoles(chefSalleO).catch(console.error);
        }


        if (message.member.roles.has(chefoD)) {
            message.member.send("Merci pour votre investissement :wink:")
            message.channel.send(message.member.user.username + " a quitté son poste de chef de la salle D d'OSHIMO, merci à lui.")
            message.member.removeRoles(chefoD).catch(console.error);
            message.member.removeRoles(chefSalleO).catch(console.error);
        }

    }


    //Chef de salle Terra Cogita acces commands

    if (message.content.substr(0, 7) == prefix + "cheft-") {
        if (message.content == prefix + "cheft-a") {
            var chefta = message.guild.roles.get(cheftA).members.map(m => m.user.id)

            if (chefta.length === 0 && message.member.roles.has(salleABT)) {
                message.channel.send("Bienvenue à notre nouveau chef de la salle A !")
                message.member.send(chefTerraMessage)
                message.member.addRoles(cheftA).catch(console.error);
                message.member.addRoles(chefSalleT).catch(console.error);
            } else if (message.member.roles.has(salleABT)) {
                message.channel.send("Il y à déjà un chef de la salle A")
            } else {
                message.channel.send("Vous ne remplissez pas les conditions pour devenir le chef de la salle A. Pour plus d'info, consultez la <#504933472536887297>")
            }
        }

        if (message.content == prefix + "cheft-b") {
            var cheftb = message.guild.roles.get(cheftB).members.map(m => m.user.id)

            if (cheftb.length === 0 && message.member.roles.has(salleABT)) {
                message.channel.send("Bienvenue à notre nouveau chef de la salle B !")
                message.member.send(chefTerraMessage)
                message.member.addRoles(cheftB).catch(console.error);
                message.member.addRoles(chefSalleT).catch(console.error);
            } else if (message.member.roles.has(salleABT)) {
                message.channel.send("Il y à déjà un chef de la salle B")
            } else {
                message.channel.send("Vous ne remplissez pas les conditions pour devenir le chef de la salle B. Pour plus d'info, consultez la <#504933472536887297>")
            }
        }

        if (message.content == prefix + "cheft-c") {
            var cheftc = message.guild.roles.get(cheftC).members.map(m => m.user.id)

            if (cheftc.length === 0 && message.member.roles.has(salleCDT)) {
                message.channel.send("Bienvenue à notre nouveau chef de la salle C !")
                message.member.send(chefTerraMessage)
                message.member.addRoles(cheftC).catch(console.error);
                message.member.addRoles(chefSalleT).catch(console.error);
            } else if (message.member.roles.has(salleCDT)) {
                message.channel.send("Il y à déjà un chef de la salle C")
            } else {
                message.channel.send("Vous ne remplissez pas les conditions pour devenir le chef de la salle C. Pour plus d'info, consultez la <#504933472536887297>")
            }
        }

        if (message.content == prefix + "cheft-d") {
            var cheftd = message.guild.roles.get(cheftD).members.map(m => m.user.id)

            if (cheftd.length === 0 && message.member.roles.has(salleCDT)) {
                message.channel.send("Bienvenue à notre nouveau chef de la salle D !")
                message.member.send(chefTerraMessage)
                message.member.addRoles(cheftD).catch(console.error);
                message.member.addRoles(chefSalleT).catch(console.error);
            } else if (message.member.roles.has(salleCDT)) {
                message.channel.send("Il y à déjà un chef de la salle D")
            } else {
                message.channel.send("Vous ne remplissez pas les conditions pour devenir le chef de la salle D. Pour plus d'info, consultez la <#504933472536887297>")
            }
        }
        var chefta = message.guild.roles.get(cheftA).members.map(m => m.user.id)
        var cheftb = message.guild.roles.get(cheftB).members.map(m => m.user.id)
        var cheftc = message.guild.roles.get(cheftC).members.map(m => m.user.id)
        var cheftd = message.guild.roles.get(cheftD).members.map(m => m.user.id)
        if (chefta.length + cheftb.length + cheftc.length + cheftd.length == 3) {
            message.member.guild.channels.get(chefRoomT).send("<@&" + chefSalleT + "> Vous êtes au complet, il faut donc choisir une date d'ouverture ! Pour proposer une date utilisez la commande `?setDate-t (date et heure)`");
        }
    }

    if (message.content == prefix + "fint") {

        if (message.member.roles.has(cheftA)) {
            message.member.send("Merci pour votre investissement :wink:")
            message.channel.send(message.member.user.username + " a quitté son poste de chef de la salle A de TERRA COGITA, merci à lui.")
            message.member.removeRoles(cheftA).catch(console.error);
            message.member.removeRoles(chefSalleT).catch(console.error);
        }

        if (message.member.roles.has(cheftB)) {
            message.member.send("Merci pour votre investissement :wink:")
            message.channel.send(message.member.user.username + " a quitté son poste de chef de la salle B de TERRA COGITA, merci à lui.")
            message.member.removeRoles(cheftB).catch(console.error);
            message.member.removeRoles(chefSalleT).catch(console.error);
        }

        if (message.member.roles.has(cheftC)) {
            message.member.send("Merci pour votre investissement :wink:")
            message.channel.send(message.member.user.username + " a quitté son poste de chef de la salle C de TERRA COGITA, merci à lui.")
            message.member.removeRoles(cheftC).catch(console.error);
            message.member.removeRoles(chefSalleT).catch(console.error);
        }

        if (message.member.roles.has(cheftD)) {
            message.member.send("Merci pour votre investissement :wink:")
            message.channel.send(message.member.user.username + " a quitté son poste de chef de la salle D de TERRA COGITA, merci à lui.")
            message.member.removeRoles(cheftD).catch(console.error);
            message.member.removeRoles(chefSalleT).catch(console.error);
        }

    }


    //Chef de salle Herdegrize acces commands

    if (message.content.substr(0, 7) == prefix + "chefh-") {
        if (message.content == prefix + "chefh-a") {
            var chefha = message.guild.roles.get(chefhA).members.map(m => m.user.id)

            if (chefha.length === 0 && message.member.roles.has(salleABH)) {
                message.channel.send("Bienvenue à notre nouveau chef de la salle A !")
                message.member.send(chefHerdegrizeMessage)
                message.member.addRoles(chefhA).catch(console.error);
                message.member.addRoles(chefSalleH).catch(console.error);
            } else if (message.member.roles.has(salleABH)) {
                message.channel.send("Il y à déjà un chef de la salle A")
            } else {
                message.channel.send("Vous ne remplissez pas les conditions pour devenir le chef de la salle A. Pour plus d'info, consultez la <#504933472536887297>")
            }
        }

        if (message.content == prefix + "chefh-b") {
            var chefhb = message.guild.roles.get(chefhB).members.map(m => m.user.id)

            if (chefhb.length === 0 && message.member.roles.has(salleABH)) {
                message.channel.send("Bienvenue à notre nouveau chef de la salle B !")
                message.member.send(chefHerdegrizeMessage)
                message.member.addRoles(chefhB).catch(console.error);
                message.member.addRoles(chefSalleH).catch(console.error);
            } else if (message.member.roles.has(salleABH)) {
                message.channel.send("Il y à déjà un chef de la salle B")
            } else {
                message.channel.send("Vous ne remplissez pas les conditions pour devenir le chef de la salle B. Pour plus d'info, consultez la <#504933472536887297>")
            }
        }

        if (message.content == prefix + "chefh-c") {
            var chefhc = message.guild.roles.get(chefhC).members.map(m => m.user.id)

            if (chefhc.length === 0 && message.member.roles.has(salleCDH)) {
                message.channel.send("Bienvenue à notre nouveau chef de la salle A !")
                message.member.send(chefHerdegrizeMessage)
                message.member.addRoles(chefhC).catch(console.error);
                message.member.addRoles(chefSalleH).catch(console.error);
            } else if (message.member.roles.has(salleCDH)) {
                message.channel.send("Il y à déjà un chef de la salle C")
            } else {
                message.channel.send("Vous ne remplissez pas les conditions pour devenir le chef de la salle C. Pour plus d'info, consultez la <#504933472536887297>")
            }
        }

        if (message.content == prefix + "chefh-d") {
            var chefhd = message.guild.roles.get(chefhD).members.map(m => m.user.id)

            if (chefhd.length === 0 && message.member.roles.has(salleCDH)) {
                message.channel.send("Bienvenue à notre nouveau chef de la salle D !")
                message.member.send(chefHerdegrizeMessage)
                message.member.addRoles([chefhD, chefSalleH]).catch(console.error);
            } else if (message.member.roles.has(salleCDH)) {
                message.channel.send("Il y à déjà un chef de la salle D")
            } else {
                message.channel.send("Vous ne remplissez pas les conditions pour devenir le chef de la salle D. Pour plus d'info, consultez la <#504933472536887297>")
            }
        }
        var chefha = message.guild.roles.get(chefhA).members.map(m => m.user.id)
        var chefhb = message.guild.roles.get(chefhB).members.map(m => m.user.id)
        var chefhc = message.guild.roles.get(chefhC).members.map(m => m.user.id)
        var chefhd = message.guild.roles.get(chefhD).members.map(m => m.user.id)
        if (chefha.length + chefhb.length + chefhc.length + chefhd.length == 3) {
            message.member.guild.channels.get(chefRoomH).send("<@&" + chefSalleH + "> Vous êtes au complet, il faut donc choisir une date d'ouverture ! Pour proposer une date utilisez la commande `?setDate-h (date et heure)`");
        }
    }

    if (message.content == prefix + "finh") {

        if (message.member.roles.has(chefhA)) {
            message.member.send("Merci pour votre investissement :wink:")
            message.channel.send(message.member.user.username + " a quitté son poste de chef de la salle A d'HERDEGRIZE', merci à lui.")
            message.member.removeRoles(chefhA).catch(console.error);
            message.member.removeRoles(chefSalleH).catch(console.error);
        }

        if (message.member.roles.has(chefhB)) {
            message.member.send("Merci pour votre investissement :wink:")
            message.channel.send(message.member.user.username + " a quitté son poste de chef de la salle B d'HERDEGRIZE', merci à lui.")
            message.member.removeRoles(chefhB).catch(console.error);
            message.member.removeRoles(chefSalleH).catch(console.error);
        }

        if (message.member.roles.has(chefhC)) {
            message.member.send("Merci pour votre investissement :wink:")
            message.channel.send(message.member.user.username + " a quitté son poste de chef de la salle C d'HERDEGRIZE', merci à lui.")
            message.member.removeRoles(chefhC).catch(console.error);
            message.member.removeRoles(chefSalleH).catch(console.error);
        }

        if (message.member.roles.has(chefhD)) {
            message.member.send("Merci pour votre investissement :wink:")
            message.channel.send(message.member.user.username + " a quitté son poste de chef de la salle D d'HERDEGRIZE', merci à lui.")
            message.member.removeRoles(chefhD).catch(console.error);
            message.member.removeRoles(chefSalleH).catch(console.error);
        }

    }


    //Reboot command for Oshimo chef


    if (message.content == prefix + "rebooto-a" && message.member.roles.has(admin)) {
        var chefoa = message.guild.roles.get(chefoA).members.map(m => m.user.id)

        if (chefoa.length == 0) {
            message.channel.send("il n'y a personne avec ce rôle.")
        } else {
            var member = message.guild.roles.get(chefoA).members
            member.forEach(members => {
                members.removeRoles([chefoA, chefSalleO])

            });
            message.channel.send("Le chef de la salle A pour le serveur Oshimo à bien été enlevé.")
        }
    }


    if (message.content == prefix + "rebooto-b" && message.member.roles.has(admin)) {
        var chefob = message.guild.roles.get(chefoB).members.map(m => m.user.id)

        if (chefob.length == 0) {
            message.channel.send("il n'y a personne avec ce rôle")
        } else {
            var member = message.guild.roles.get(chefoB).members
            member.forEach(members => {
                members.removeRoles([chefoB, chefSalleO])

            });
            message.channel.send("Le chef de la salle B pour le serveur Oshimo a bien été enlevé.")
        }
    }


    if (message.content == prefix + "rebooto-c" && message.member.roles.has(admin)) {
        var chefoc = message.guild.roles.get(chefoC).members.map(m => m.user.id)

        if (chefoc.length == 0) {
            message.channel.send("il n'y a personne avec ce rôle")
        } else {
            var member = message.guild.roles.get(chefoC).members
            member.forEach(members => {
                members.removeRoles([chefoC, chefSalleO])

            });
            message.channel.send("Le chef de la salle C pour le serveur Oshimo a bien été enlevé.")
        }
    }


    if (message.content == prefix + "rebooto-d" && message.member.roles.has(admin)) {
        var chefod = message.guild.roles.get(chefoD).members.map(m => m.user.id)

        if (chefod.length == 0) {
            message.channel.send("il n'y a personne avec ce rôle")
        } else {
            var member = message.guild.roles.get(chefoD).members
            member.forEach(members => {
                members.removeRoles([chefoD, chefSalleO])

            });
            message.channel.send("Le chef de la salle D pour le serveur Oshimo a bien été enlevé.")
        }
    }


    //Reboot command for Terra chef


    if (message.content == prefix + "reboott-a" && message.member.roles.has(admin)) {
        var chefta = message.guild.roles.get(cheftA).members.map(m => m.user.id)

        if (chefta.length == 0) {
            message.channel.send("il n'y a personne avec ce rôle")
        } else {
            var member = message.guild.roles.get(cheftA).members
            member.forEach(members => {
                members.removeRoles([cheftA, chefSalleT])

            });
            message.channel.send("Le chef de la salle A pour le serveur Terra Cogita a bien été enlevé.")
        }
    }


    if (message.content == prefix + "reboott-b" && message.member.roles.has(admin)) {
        var cheftb = message.guild.roles.get(cheftB).members.map(m => m.user.id)

        if (cheftb.length == 0) {
            message.channel.send("il n'y a personne avec ce rôle")
        } else {
            var member = message.guild.roles.get(cheftB).members
            member.forEach(members => {
                members.removeRoles([cheftB, chefSalleT])

            });
            message.channel.send("Le chef de la salle B pour le serveur Terra Cogita a bien été enlevé.")
        }
    }


    if (message.content == prefix + "reboott-c" && message.member.roles.has(admin)) {
        var cheftc = message.guild.roles.get(cheftC).members.map(m => m.user.id)

        if (cheftc.length == 0) {
            message.channel.send("il n'y a personne avec ce rôle")
        } else {
            var member = message.guild.roles.get(cheftC).members
            member.forEach(members => {
                members.removeRoles([cheftC, chefSalleT])

            });
            message.channel.send("Le chef de la salle C pour le serveur Terra Cogita a bien été enlevé.")
        }
    }


    if (message.content == prefix + "reboott-d" && message.member.roles.has(admin)) {
        var cheftd = message.guild.roles.get(cheftD).members.map(m => m.user.id)

        if (cheftd.length == 0) {
            message.channel.send("il n'y a personne avec ce rôle")
        } else {
            var member = message.guild.roles.get(cheftD).members
            member.forEach(members => {
                members.removeRoles([cheftD, chefSalleT])

            });
            message.channel.send("Le chef de la salle D pour le serveur Terra Cogita a bien été enlevé.")
        }
    }


    //Reboot command for Herdegrize chef


    if (message.content == prefix + "rebooth-a" && message.member.roles.has(admin)) {
        var chefha = message.guild.roles.get(chefhA).members.map(m => m.user.id)

        if (chefha.length == 0) {
            message.channel.send("il n'y a personne avec ce rôle")
        } else {
            var member = message.guild.roles.get(chefhA).members
            member.forEach(members => {
                members.removeRoles([chefhA, chefSalleH])

            });
            message.channel.send("Le chef de la salle A pour le serveur Herdegrize a bien été enlevé.")
        }
    }


    if (message.content == prefix + "rebooth-b" && message.member.roles.has(admin)) {
        var chefhb = message.guild.roles.get(chefhB).members.map(m => m.user.id)

        if (chefhb.length == 0) {
            message.channel.send("il n'y a personne avec ce rôle")
        } else {
            var member = message.guild.roles.get(chefhB).members
            member.forEach(members => {
                members.removeRoles([chefhB, chefSalleH])

            });
            message.channel.send("Le chef de la salle B pour le serveur Herdegrize a bien été enlevé.")
        }
    }


    if (message.content == prefix + "rebooth-c" && message.member.roles.has(admin)) {
        var chefhc = message.guild.roles.get(chefhC).members.map(m => m.user.id)

        if (chefhc.length == 0) {
            message.channel.send("il n'y a personne avec ce rôle")
        } else {
            var member = message.guild.roles.get(chefhC).members
            member.forEach(members => {
                members.removeRoles([chefhC, chefSalleH])

            });
            message.channel.send("Le chef de la salle C pour le serveur Herdegrize a bien été enlevé.")
        }
    }


    if (message.content == prefix + "rebooth-d" && message.member.roles.has(admin)) {
        var chefhd = message.guild.roles.get(chefhD).members.map(m => m.user.id)

        if (chefhd.length == 0) {
            message.channel.send("il n'y a personne avec ce rôle")
        } else {
            var member = message.guild.roles.get(chefhD).members
            member.forEach(members => {
                members.removeRoles([chefhD, chefSalleH])

            });
            message.channel.send("Le chef de la salle D pour le serveur Herdegrize a bien été enlevé.")
        }
    }





    //Command for choose and validate a date of opening for Oshimo


    if (message.content.substring(0, 10) == prefix + "setDate-o" || message.content.substring(0, 10) == prefix + "setdate-o") {

        if (message.member.roles.has(chefSalleO)) {
            dateOprov = message.content.substring(11)
            chefoaValid = false
            chefobValid = false
            chefocValid = false
            chefodValid = false
            message.channel.send("<@&" + chefSalleO + "> La date du " + dateOprov + " a bien été enregistrée. Si vous êtes d'accord pour cette date faites la commande `?confirmDate`. Si vous n'êtes pas disponible, écrivez `?cancelDate-o` et proposez une date qui vous convient.")
            dateO = "Les chefs du serveur OSHIMO se concertent pour choisir une date, vous serez bientôt tenu au courant !"
        }
        if (message.member.roles.has(chefoA)) {
            chefoaValid = true
        }
        if (message.member.roles.has(chefoB)) {
            chefobValid = true
        }
        if (message.member.roles.has(chefoC)) {
            chefocValid = true
        }
        if (message.member.roles.has(chefoD)) {
            chefodValid = true
        }
    }



    if (message.content == prefix + "viewDate-o" || message.content == prefix + "viewdate-o") {

        var chefoa = message.guild.roles.get(chefoA).members.map(m => m.user.id)
        var chefob = message.guild.roles.get(chefoB).members.map(m => m.user.id)
        var chefoc = message.guild.roles.get(chefoC).members.map(m => m.user.id)
        var chefod = message.guild.roles.get(chefoD).members.map(m => m.user.id)

        if (chefoa.length > 0 && chefob.length > 0 && chefoc.length > 0 && chefod.length > 0) {
            message.channel.send(dateO)
        } else {
            message.channel.send("Le serveur OSHIMO n'a pas assez de chef pour organiser une ouverture pour le moment. N'hésitez pas à aller lire <#" + devenirChef + "> !")
        }

    }
    if (message.content == prefix + "viewDateProv-o" || message.content == prefix + "viewdateprov-o") {
        if (dateOprov == "") {
            message.channel.send("Il n'y a pas de date d'ouverture proposée pour le moment.")
        } else {
            message.channel.send("La date proposée est le " + dateOprov)
        }
    }

    if (message.content == prefix + "cancelDate-o" && (message.member.roles.has(chefSalleO) || message.member.roles.has(admin))) {

        chefoaValid = false
        chefobValid = false
        chefocValid = false
        chefodValid = false
        dateO = "Il n'y a pas de date d'ouverture prévu pour l'instant"
        dateOprov = ""
        if (message.member.roles.has(admin)) {
            message.channel.send("J'ai réinitialiser la date d'ouverture d'Oshimo")
        } else {
            message.channel.send("Votre indisponibilité a bien été prise en compte")
        }
    }

    if (message.content == prefix + "confirmDate" && message.member.roles.has(chefSalleO)) {
        var chefoan = message.guild.roles.get(chefoA).members.map(m => m.user.username)
        var chefobn = message.guild.roles.get(chefoB).members.map(m => m.user.username)
        var chefocn = message.guild.roles.get(chefoC).members.map(m => m.user.username)
        var chefodn = message.guild.roles.get(chefoD).members.map(m => m.user.username)
        countValid = 0
        if (chefoaValid == false) {
            countValid == countValid++
        }
        if (chefobValid == false) {
            countValid == countValid++
        }
        if (chefocValid == false) {
            countValid == countValid++
        }
        if (chefodValid == false) {
            countValid == countValid++
        }
        if (message.member.roles.has(chefoA)) {
            if (chefoaValid == true) {
                message.channel.send("Tu à déjà confirmé ta présence")
            } else {
                chefoaValid = true
                countValid = countValid - 1
                if (countValid == 0) {
                    message.channel.send("La date est confirmée merci de votre engagement !")
                } else if (countValid == 1) {

                    if (chefobValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefoB + "> (" + chefobn + ") qui doit confirmer la date.")
                    }
                    if (chefocValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefoC + "> (" + chefocn + ") qui doit confirmer la date.")
                    }
                    if (chefodValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefoD + "> (" + chefodn + ") qui doit confirmer la date.")
                    }

                } else {
                    message.channel.send("Merci de ta confirmation, il reste " + countValid + "chefs qui doivent confirmer la date")
                    if (chefobValid == false) {
                        message.channel.send("<@" + chefoB + "> (" + chefobn + ") merci de confirmer ta présence.")
                    }
                    if (chefocValid == false) {
                        message.channel.send("<@" + chefoC + "> (" + chefocn + ") merci de confirmer ta présence.")
                    }
                    if (chefodValid == false) {
                        message.channel.send("<@" + chefoD + "> (" + chefodn + ") merci de confirmer ta présence.")
                    }
                }
            }
        }
        if (message.member.roles.has(chefoB)) {
            if (chefobValid == true) {
                message.channel.send("Tu à déjà confirmé ta présence")
            } else {
                chefobValid = true
                countValid = countValid - 1
                if (countValid == 0) {
                    message.channel.send("La date est confirmée merci de votre engagement !")
                } else if (countValid == 1) {
                    if (chefoaValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefoA + "> (" + chefoan + ") qui doit confirmer la date.")
                    }
                    if (chefocValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefoC + "> (" + chefocn + ") qui doit confirmer la date.")
                    }
                    if (chefodValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefoD + "> (" + chefodn + ") qui doit confirmer la date.")
                    }

                } else {
                    message.channel.send("Merci de ta confirmation, il reste " + countValid + " chefs qui doivent confirmer la date")
                    if (chefoaValid == false) {
                        message.channel.send("<@" + chefoA + "> (" + chefoan + ") merci de confirmer ta présence.")
                    }
                    if (chefocValid == false) {
                        message.channel.send("<@" + chefoC + "> (" + chefocn + ") merci de confirmer ta présence.")
                    }
                    if (chefodValid == false) {
                        message.channel.send("<@" + chefoD + "> (" + chefodn + ") merci de confirmer ta présence.")
                    }

                }
            }
        }
        if (message.member.roles.has(chefoC)) {
            if (chefocValid == true) {
                message.channel.send("Tu à déjà confirmé ta présence")
            } else {
                chefocValid = true
                countValid = countValid - 1
                if (countValid == 0) {
                    message.channel.send("La date est confirmée merci de votre engagement !")
                } else if (countValid == 1) {
                    if (chefoaValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefoA + "> (" + chefoan + ") qui doit confirmer la date.")
                    }
                    if (chefobValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefoB + "> (" + chefobn + ") qui doit confirmer la date.")
                    }
                    if (chefodValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefoD + "> (" + chefodn + ") qui doit confirmer la date.")
                    }
                } else {
                    message.channel.send("Merci de ta confirmation, il reste " + countValid + " chefs qui doivent confirmer la date")
                    if (chefoaValid == false) {
                        message.channel.send("<@" + chefoA + "> (" + chefoan + ") merci de confirmer ta présence.")
                    }
                    if (chefobValid == false) {
                        message.channel.send("<@" + chefoB + "> (" + chefobn + ") merci de confirmer ta présence.")
                    }
                    if (chefodValid == false) {
                        message.channel.send("<@" + chefoD + "> (" + chefodn + ") merci de confirmer ta présence.")
                    }

                }
            }
        }
        if (message.member.roles.has(chefoD)) {
            if (chefodValid == true) {
                message.channel.send("Tu à déjà confirmé ta présence")
            } else {
                chefodValid = true
                countValid = countValid - 1
                if (countValid == 0) {
                    message.channel.send("La date est confirmée merci de votre engagement !")
                } else if (countValid == 1) {
                    if (chefoaValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefoA + "> (" + chefoan + ") qui doit confirmer la date.")
                    }
                    if (chefobValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefoB + "> (" + chefobn + ") qui doit confirmer la date.")
                    }
                    if (chefocValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefoC + "> (" + chefocn + ") qui doit confirmer la date.")
                    }
                } else {
                    message.channel.send("Merci de ta confirmation, il reste " + countValid + " chef qui doit confirmer la date")
                    if (chefoaValid == false) {
                        message.channel.send("<@" + chefoA + "> (" + chefoan + ") merci de confirmer ta présence.")
                    }
                    if (chefobValid == false) {
                        message.channel.send("<@" + chefoB + "> (" + chefobn + ") merci de confirmer ta présence.")
                    }
                    if (chefocValid == false) {
                        message.channel.send("<@" + chefoC + "> (" + chefocn + ") merci de confirmer ta présence.")
                    }
                }
            }
        }

        if (chefoaValid === true && chefobValid === true && chefocValid === true && chefodValid === true) {
            dateO = "La prochaine ouverture sur le serveur OSHIMO est prévu le : " + dateOprov
            message.member.guild.channels.get(oshimoAnnoucementChannel).send("<@&" + oshimoRole + ">" + " Bonne nouvelle, une date a été choisie ! L'ouverture aura lieu le " + dateO);
        }
    }

    if (message.content == prefix + "viewConfirm-o" || message.content == prefix + "viewconfirm-o") {
        var chefoan = message.guild.roles.get(chefoA).members.map(m => m.user.username)
        var chefobn = message.guild.roles.get(chefoB).members.map(m => m.user.username)
        var chefocn = message.guild.roles.get(chefoC).members.map(m => m.user.username)
        var chefodn = message.guild.roles.get(chefoD).members.map(m => m.user.username)
        if (dateOprov == "") {
            message.channel.send("Il n'y à pas de date de proposée pour le moment")
        } else {
            if (chefoaValid == false) {
                message.channel.send(chefoan + " n'a pas encore validé")
            }
            if (chefobValid == false) {
                message.channel.send(chefobn + " n'a pas encore validé")
            }
            if (chefocValid == false) {
                message.channel.send(chefocn + " n'a pas encore validé")
            }
            if (chefodValid == false) {
                message.channel.send(chefodn + " n'a pas encore validé")
            }
        }
    }


    //Command for choose and validate a date of opening for Terra Cogita


    if (message.content.substring(0, 10) == prefix + "setDate-t" || message.content.substring(0, 10) == prefix + "setdate-t") {

        if (message.member.roles.has(chefSalleT)) {
            dateTprov = message.content.substring(12)
            cheftaValid = false
            cheftbValid = false
            cheftcValid = false
            cheftdValid = false
            message.channel.send("<@&" + chefSalleT + "> La date du " + dateTprov + " a bien été enregistrée. Si vous êtes d'accord pour cette date faites la commande `?confirmDate`. Si vous n'êtes pas disponible, écrivez `?cancelDate-t` et proposez une date qui vous convient.")
            dateT = "Les chefs du serveur TERRA COGITA se concertent pour choisir une date, vous serez bientôt tenu au courant !"
        }
        if (message.member.roles.has(cheftA)) {
            cheftaValid = true
        }
        if (message.member.roles.has(cheftB)) {
            cheftbValid = true
        }
        if (message.member.roles.has(cheftC)) {
            cheftcValid = true
        }
        if (message.member.roles.has(cheftD)) {
            cheftdValid = true
        }
    }



    if (message.content == prefix + "viewDate-t" || message.content == prefix + "viewdate-t") {
        var chefta = message.guild.roles.get(cheftA).members.map(m => m.user.id)
        var cheftb = message.guild.roles.get(cheftB).members.map(m => m.user.id)
        var cheftc = message.guild.roles.get(cheftC).members.map(m => m.user.id)
        var cheftd = message.guild.roles.get(cheftD).members.map(m => m.user.id)

        if (chefta.length > 0 && cheftb.length > 0 && cheftc.length > 0 && cheftd.length > 0) {
            message.channel.send(dateT)
        } else {
            message.channel.send("Le serveur TERRA COGITA n'a pas assez de chef pour organiser une ouverture pour le moment. N'hésitez pas à aller lire <#" + devenirChef + "> !")
        }
    }
    if (message.content == prefix + "viewDateProv-t" || message.content == prefix + "viewdateprov-t") {
        if (dateTprov == "") {
            "Il n'y a pas de date d'ouverture proposée pour le moment."
        } else {
            message.channel.send("La date proposée est le " + dateTprov)
        }
    }

    if (message.content == prefix + "cancelDate-t" && (message.member.roles.has(chefSalleT) || message.member.roles.has(admin))) {

        cheftaValid = false
        cheftbValid = false
        cheftcValid = false
        cheftdValid = false
        dateT = "Il n'y a pas de date d'ouverture prévu pour l'instant."
        dateTprov = ""
        if (message.member.roles.has(admin)) {
            message.channel.send("J'ai réinitialiser la date d'ouverture de Terra Cogita.")
        } else {
            message.channel.send("Votre indisponibilité a bien été prise en compte.")
        }
    }

    if (message.content == prefix + "confirmDate" && message.member.roles.has(chefSalleT)) {
        var cheftan = message.guild.roles.get(cheftA).members.map(m => m.user.username)
        var cheftbn = message.guild.roles.get(cheftB).members.map(m => m.user.username)
        var cheftcn = message.guild.roles.get(cheftC).members.map(m => m.user.username)
        var cheftdn = message.guild.roles.get(cheftD).members.map(m => m.user.username)
        countValid = 0
        if (cheftaValid == false) {
            countValid == countValid++
        }
        if (cheftbValid == false) {
            countValid == countValid++
        }
        if (cheftcValid == false) {
            countValid == countValid++
        }
        if (cheftdValid == false) {
            countValid == countValid++
        }
        if (message.member.roles.has(cheftA)) {
            if (cheftaValid == true) {
                message.channel.send("Tu à déjà confirmé ta présence")
            } else {
                cheftaValid = true
                countValid = countValid - 1
                if (countValid == 0) {
                    message.channel.send("La date est confirmée merci de votre engagement !")
                } else if (countValid == 1) {

                    if (cheftbValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + cheftB + "> (" + cheftbn + ") qui doit confirmer la date.")
                    }
                    if (cheftcValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + cheftC + "> (" + cheftcn + ") qui doit confirmer la date.")
                    }
                    if (cheftdValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + cheftD + "> (" + cheftdn + ") qui doit confirmer la date.")
                    }

                } else {
                    message.channel.send("Merci de ta confirmation, il reste " + countValid + "chefs qui doivent confirmer la date.")
                    if (cheftbValid == false) {
                        message.channel.send("<@" + cheftB + "> (" + cheftbn + ") merci de confirmer ta présence.")
                    }
                    if (cheftcValid == false) {
                        message.channel.send("<@" + cheftC + "> (" + cheftcn + ") merci de confirmer ta présence.")
                    }
                    if (cheftdValid == false) {
                        message.channel.send("<@" + cheftD + "> (" + cheftdn + ") merci de confirmer ta présence.")
                    }
                }
            }
        }
        if (message.member.roles.has(cheftB)) {
            if (cheftbValid == true) {
                message.channel.send("Tu à déjà confirmé ta présence")
            } else {
                cheftbValid = true
                countValid = countValid - 1
                if (countValid == 0) {
                    message.channel.send("La date est confirmée merci de votre engagement !")
                } else if (countValid == 1) {
                    if (cheftaValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + cheftA + "> (" + cheftan + ") qui doit confirmer la date.")
                    }
                    if (cheftcValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + cheftC + "> (" + cheftcn + ") qui doit confirmer la date.")
                    }
                    if (cheftdValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + cheftD + "> (" + cheftdn + ") qui doit confirmer la date.")
                    }

                } else {
                    message.channel.send("Merci de ta confirmation, il reste " + countValid + " chefs qui doivent confirmer la date")
                    if (cheftaValid == false) {
                        message.channel.send("<@" + cheftA + "> (" + cheftan + ") merci de confirmer ta présence.")
                    }
                    if (cheftcValid == false) {
                        message.channel.send("<@" + cheftC + "> (" + cheftcn + ") merci de confirmer ta présence.")
                    }
                    if (cheftdValid == false) {
                        message.channel.send("<@" + cheftD + "> (" + cheftdn + ") merci de confirmer ta présence.")
                    }

                }
            }
        }
        if (message.member.roles.has(cheftC)) {
            if (cheftcValid == true) {
                message.channel.send("Tu à déjà confirmé ta présence")
            } else {
                cheftcValid = true
                countValid = countValid - 1
                if (countValid == 0) {
                    message.channel.send("La date est confirmée merci de votre engagement !")
                } else if (countValid == 1) {
                    if (cheftaValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + cheftA + "> (" + cheftan + ") qui doit confirmer la date.")
                    }
                    if (cheftbValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + cheftB + "> (" + cheftbn + ") qui doit confirmer la date.")
                    }
                    if (cheftdValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + cheftD + "> (" + cheftdn + ") qui doit confirmer la date.")
                    }
                } else {
                    message.channel.send("Merci de ta confirmation, il reste " + countValid + " chefs qui doivent confirmer la date")
                    if (cheftaValid == false) {
                        message.channel.send("<@" + cheftA + "> (" + cheftan + ") merci de confirmer ta présence.")
                    }
                    if (cheftbValid == false) {
                        message.channel.send("<@" + cheftB + "> (" + cheftbn + ") merci de confirmer ta présence.")
                    }
                    if (cheftdValid == false) {
                        message.channel.send("<@" + cheftD + "> (" + cheftdn + ") merci de confirmer ta présence.")
                    }

                }
            }
        }
        if (message.member.roles.has(cheftD)) {
            if (cheftdValid == true) {
                message.channel.send("Tu à déjà confirmé ta présence")
            } else {
                cheftdValid = true
                countValid = countValid - 1
                if (countValid == 0) {
                    message.channel.send("La date est confirmée merci de votre engagement !")
                } else if (countValid == 1) {
                    if (cheftaValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + cheftA + "> (" + cheftan + ") qui doit confirmer la date.")
                    }
                    if (cheftbValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + cheftB + "> (" + cheftbn + ") qui doit confirmer la date.")
                    }
                    if (cheftcValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + cheftC + "> (" + cheftcn + ") qui doit confirmer la date.")
                    }
                } else {
                    message.channel.send("Merci de ta confirmation, il reste " + countValid + " chef qui doit confirmer la date")
                    if (cheftaValid == false) {
                        message.channel.send("<@" + cheftA + "> (" + cheftan + ") merci de confirmer ta présence.")
                    }
                    if (cheftbValid == false) {
                        message.channel.send("<@" + cheftB + "> (" + cheftbn + ") merci de confirmer ta présence.")
                    }
                    if (cheftcValid == false) {
                        message.channel.send("<@" + cheftC + "> (" + cheftcn + ") merci de confirmer ta présence.")
                    }
                }
            }
        }

        if (cheftaValid === true && cheftbValid === true && cheftcValid === true && cheftdValid === true) {
            dateT = "La prochaine ouverture sur le serveur TERRA COGITA est prévu le : " + dateTprov
            message.member.guild.channels.get(terraAnnoucementChannel).send("<@&" + terraRole + ">" + " Bonne nouvelle, une date a été choisie ! L'ouverture aura lieu le " + dateT + ".");
        }
    }

    if (message.content == prefix + "viewConfirm-t" || message.content == prefix + "viewConfirm-t") {
        var cheftan = message.guild.roles.get(cheftA).members.map(m => m.user.username)
        var cheftbn = message.guild.roles.get(cheftB).members.map(m => m.user.username)
        var cheftcn = message.guild.roles.get(cheftC).members.map(m => m.user.username)
        var cheftdn = message.guild.roles.get(cheftD).members.map(m => m.user.username)
        if (dateTprov == "") {
            message.channel.send("Il n'y à pas de date de proposée pour le moment.")
        } else {
            if (cheftaValid == false) {
                message.channel.send(cheftan + " n'a pas encore validé.")
            }
            if (cheftbValid == false) {
                message.channel.send(cheftbn + " n'a pas encore validé.")
            }
            if (cheftcValid == false) {
                message.channel.send(cheftcn + " n'a pas encore validé.")
            }
            if (cheftdValid == false) {
                message.channel.send(cheftdn + " n'a pas encore validé.")
            }
        }
    }

    //Command for choose and validate a date of opening for Herdegrize


    if (message.content.substring(0, 10) == prefix + "setDate-h" || message.content.substring(0, 10) == prefix + "setdate-h") {

        if (message.member.roles.has(chefSalleH)) {
            dateTprov = message.content.substring(11)
            chefhaValid = false
            chefhbValid = false
            chefhcValid = false
            chefhdValid = false
            message.channel.send("<@&" + chefSalleH + "> La date du " + dateHprov + " a bien été enregistrée. Si vous êtes d'accord pour cette date faites la commande `!confirmDate`. Si vous n'êtes pas disponible, écrivez `!cancelDate-h` et proposez une date qui vous convient.")
            dateH = "les chefs du serveur HERDEGRIZE se concertent pour choisir une date, vous serez bientôt tenu au courant !"
        }
        if (message.member.roles.has(chefhA)) {
            chefhaValid = true
        }
        if (message.member.roles.has(chefhB)) {
            chefhbValid = true
        }
        if (message.member.roles.has(chefhC)) {
            chefhcValid = true
        }
        if (message.member.roles.has(chefhD)) {
            chefhdValid = true
        }
    }



    if (message.content == prefix + "viewDate-h" || message.content == prefix + "viewdate-h") {
        var chefha = message.guild.roles.get(chefhA).members.map(m => m.user.id)
        var chefhb = message.guild.roles.get(chefhB).members.map(m => m.user.id)
        var chefhc = message.guild.roles.get(chefhC).members.map(m => m.user.id)
        var chefhd = message.guild.roles.get(chefhD).members.map(m => m.user.id)

        if (chefha.length > 0 && chefhb.length > 0 && chefhc.length > 0 && chefhd.length > 0) {
            message.channel.send(dateH)
        } else {
            message.channel.send("Le serveur HERDEGRIZE n'a pas assez de chef pour organiser une ouverture pour le moment. N'hésitez pas à aller lire <#" + devenirChef + "> !")
        }
    }
    if (message.content == prefix + "viewDateProv-h" || message.content == prefix + "viewdateprov-h") {
        if (dateHprov == "") {
            "Il n'y a pas de date d'ouverture proposée pour le moment."
        } else {
            message.channel.send("La date proposée est le " + dateHprov + ".")
        }
    }

    if (message.content == prefix + "cancelDate-h" && (message.member.roles.has(chefSalleH) || message.member.roles.has(admin))) {

        chefhaValid = false
        chefhbValid = false
        chefhcValid = false
        chefhdValid = false
        dateH = "Il n'y a pas de date d'ouverture prévu pour l'instant."
        dateHprov = ""
        if (message.member.roles.has(admin)) {
            message.channel.send("J'ai réinitialiser la date d'ouverture d'Herdegrize.")
        } else {
            message.channel.send("Votre indisponibilité a bien été prise en compte.")
        }
    }

    if (message.content == prefix + "confirmDate" && message.member.roles.has(chefSalleH)) {
        var chefhan = message.guild.roles.get(chefhA).members.map(m => m.user.username)
        var chefhbn = message.guild.roles.get(chefhB).members.map(m => m.user.username)
        var chefhcn = message.guild.roles.get(chefhC).members.map(m => m.user.username)
        var chefhdn = message.guild.roles.get(chefhD).members.map(m => m.user.username)
        countValid = 0
        if (chefhaValid == false) {
            countValid == countValid++
        }
        if (chefhbValid == false) {
            countValid == countValid++
        }
        if (chefhcValid == false) {
            countValid == countValid++
        }
        if (chefhdValid == false) {
            countValid == countValid++
        }
        if (message.member.roles.has(chefhA)) {
            if (chefhaValid == true) {
                message.channel.send("Tu à déjà confirmé ta présence")
            } else {
                chefhaValid = true
                countValid = countValid - 1
                if (countValid == 0) {
                    message.channel.send("La date est confirmée merci de votre engagement !")
                } else if (countValid == 1) {

                    if (chefhbValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefhB + "> (" + chefhbn + ") qui doit confirmer la date.")
                    }
                    if (chefhcValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefhC + "> (" + chefhcn + ") qui doit confirmer la date.")
                    }
                    if (chefhdValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefhD + "> (" + chefhdn + ") qui doit confirmer la date.")
                    }

                } else {
                    message.channel.send("Merci de ta confirmation, il reste " + countValid + "chefs qui doivent confirmer la date")
                    if (chefhbValid == false) {
                        message.channel.send("<@" + chefhB + "> (" + chefhbn + ") merci de confirmer ta présence.")
                    }
                    if (chefhcValid == false) {
                        message.channel.send("<@" + chefhC + "> (" + chefhcn + ") merci de confirmer ta présence.")
                    }
                    if (chefhdValid == false) {
                        message.channel.send("<@" + chefhD + "> (" + chefhdn + ") merci de confirmer ta présence.")
                    }
                }
            }
        }
        if (message.member.roles.has(chefhB)) {
            if (chefhbValid == true) {
                message.channel.send("Tu à déjà confirmé ta présence")
            } else {
                chefhbValid = true
                countValid = countValid - 1
                if (countValid == 0) {
                    message.channel.send("La date est confirmée merci de votre engagement !")
                } else if (countValid == 1) {
                    if (chefhaValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefhA + "> (" + chefhan + ") qui doit confirmer la date.")
                    }
                    if (chefhcValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefhC + "> (" + chefhcn + ") qui doit confirmer la date.")
                    }
                    if (chefhdValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefhD + "> (" + chefhdn + ") qui doit confirmer la date.")
                    }

                } else {
                    message.channel.send("Merci de ta confirmation, il reste " + countValid + " chefs qui doivent confirmer la date")
                    if (chefhaValid == false) {
                        message.channel.send("<@" + chefhA + "> (" + chefhan + ") merci de confirmer ta présence.")
                    }
                    if (chefhcValid == false) {
                        message.channel.send("<@" + chefhC + "> (" + chefhcn + ") merci de confirmer ta présence.")
                    }
                    if (chefhdValid == false) {
                        message.channel.send("<@" + chefhD + "> (" + chefhdn + ") merci de confirmer ta présence.")
                    }

                }
            }
        }
        if (message.member.roles.has(chefhC)) {
            if (chefhcValid == true) {
                message.channel.send("Tu à déjà confirmé ta présence")
            } else {
                chefhcValid = true
                countValid = countValid - 1
                if (countValid == 0) {
                    message.channel.send("La date est confirmée merci de votre engagement !")
                } else if (countValid == 1) {
                    if (chefhaValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefhA + "> (" + chefhan + ") qui doit confirmer la date.")
                    }
                    if (chefhbValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefhB + "> (" + chefhbn + ") qui doit confirmer la date.")
                    }
                    if (chefhdValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefhD + "> (" + chefhdn + ") qui doit confirmer la date.")
                    }
                } else {
                    message.channel.send("Merci de ta confirmation, il reste " + countValid + " chefs qui doivent confirmer la date")
                    if (chefhaValid == false) {
                        message.channel.send("<@" + chefhA + "> (" + chefhan + ") merci de confirmer ta présence.")
                    }
                    if (chefhbValid == false) {
                        message.channel.send("<@" + chefhB + "> (" + chefhbn + ") merci de confirmer ta présence.")
                    }
                    if (chefhdValid == false) {
                        message.channel.send("<@" + chefhD + "> (" + chefhdn + ") merci de confirmer ta présence.")
                    }

                }
            }
        }
        if (message.member.roles.has(chefhD)) {
            if (chefhdValid == true) {
                message.channel.send("Tu à déjà confirmé ta présence")
            } else {
                chefhdValid = true
                countValid = countValid - 1
                if (countValid == 0) {
                    message.channel.send("La date est confirmée merci de votre engagement !")
                } else if (countValid == 1) {
                    if (chefhaValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefhA + "> (" + chefhan + ") qui doit confirmer la date.")
                    }
                    if (chefhbValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefhB + "> (" + chefhbn + ") qui doit confirmer la date.")
                    }
                    if (chefhcValid == false) {
                        message.channel.send("Merci de ta confirmation, il ne reste plus que <@" + chefhC + "> (" + chefhcn + ") qui doit confirmer la date.")
                    }
                } else {
                    message.channel.send("Merci de ta confirmation, il reste " + countValid + " chef qui doit confirmer la date.")
                    if (chefhaValid == false) {
                        message.channel.send("<@" + chefhA + "> (" + chefhan + ") merci de confirmer ta présence.")
                    }
                    if (chefhbValid == false) {
                        message.channel.send("<@" + chefhB + "> (" + chefhbn + ") merci de confirmer ta présence.")
                    }
                    if (chefhcValid == false) {
                        message.channel.send("<@" + chefhC + "> (" + chefhcn + ") merci de confirmer ta présence.")
                    }
                }
            }
        }

        if (chefhaValid === true && chefhbValid === true && chefhcValid === true && chefhdValid === true) {
            dateH = "La prochaine ouverture sur le serveur HERDEGRIZE est prévu le : " + dateHprov
            message.member.guild.channels.get(herdegrizeAnnoucementChannel).send("<@&" + herdegrizeRole + ">" + " Bonne nouvelle, une date a été choisie ! L'ouverture aura lieu le " + dateH + ".");
        }
    }

    if (message.content == prefix + "viewConfirm-h" || message.content == prefix + "viewconfirm-h") {
        var chefhan = message.guild.roles.get(chefhA).members.map(m => m.user.username)
        var chefhbn = message.guild.roles.get(chefhB).members.map(m => m.user.username)
        var chefhcn = message.guild.roles.get(chefhC).members.map(m => m.user.username)
        var chefhdn = message.guild.roles.get(chefhD).members.map(m => m.user.username)
        if (dateHprov == "") {
            message.channel.send("Il n'y à pas de date de proposée pour le moment.")
        } else {
            if (chefhaValid == false) {
                message.channel.send(chefhan + " n'a pas encore validé.")
            }
            if (chefhbValid == false) {
                message.channel.send(chefhbn + " n'a pas encore validé.")
            }
            if (chefhcValid == false) {
                message.channel.send(chefhcn + " n'a pas encore validé.")
            }
            if (chefhdValid == false) {
                message.channel.send(chefhdn + " n'a pas encore validé.")
            }
        }
    }


    // Command for delete manually a date of opening 


    if (message.content == prefix + "deleteDate-o" && message.member.roles.has(admin)) {
        dateO = "Il n'y a pas de date d'ouverture prévu pour l'instant."
        dateOprov = ""
        message.channel.send("J'ai bien supprimé la date d'ouverture pour le serveur Oshimo")
    }


    if (message.content == prefix + "deleteDate-t" && message.member.roles.has(admin)) {
        dateO = "Il n'y a pas de date d'ouverture prévu pour l'instant."
        dateOprov = ""
        message.channel.send("J'ai bien supprimé la date d'ouverture pour le serveur Terra Cogita")
    }


    if (message.content == prefix + "deleteDate-h" && message.member.roles.has(admin)) {
        dateO = "Il n'y a pas de date d'ouverture prévu pour l'instant."
        dateOprov = ""
        message.channel.send("J'ai bien supprimé la date d'ouverture pour le serveur Herdegrize")
    }


});

bot.login('NjMxODc1Mzg5OTI4NTA1MzU3.XlggsQ.M72IjLLkSS94Wz7cZndWfnASBHk')

